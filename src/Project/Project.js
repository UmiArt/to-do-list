import React from 'react';

import './Project.css'

const List = props => {
    return (
        <div className="divList">
            <input  className="checkBox" type="checkbox"/>
            <p className="message">{props.id} . {props.message}</p>
            <button className="btn" onClick={() => props.deleteMessage(props.id)}>Delete</button>
        </div>
    )
}

export default List;