import List from "./Project/Project";
import {useState} from "react";

const App = () => {

    const [list, setList] = useState( [
        {id: 1, message: "Buy milk"},
        {id: 2, message: "Do homework"},
        {id: 3, message: "Walk with dog"},
    ]);

    const addSubmit = (e, list, setList,  input, setInput) => {
        e.preventDefault();

        const id = (list.length) ? list[list.length - 1].id + 1 : 0;

        setList([...list, {id: id, message: input}]);
        setInput('');
    };

    const deleteMessage = (id, list, setList) => {
        setList(list.filter(note => note.id !== id));
    };

    const [input, setInput] = useState( '');

    const newArray = list.map(task => (
        <List key={task.id}
              id={task.id}
              message={task.message}
              deleteMessage={(id) => deleteMessage(id, list, setList)}
        />
    ));

    return (
        <div className="to-do">
            <h2 className="title">My To Do List</h2>
            <form className="form-to-do"
                  onSubmit={(e) => addSubmit(e, list,setList, input, setInput)}>
                <input className="input" type="text" placeholder="Add new task" value={input}
                             onChange={(e) => setInput(e.target.value)}  />
                <button className="btnAddTask">Add</button>
            </form>
            {newArray}
        </div>
    )
};

export default App;
